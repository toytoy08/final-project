import React, { Component } from 'react';
import { Layout, Menu, Icon, Spin, Modal, Button, message, Input } from 'antd';
import { connect } from 'react-redux'

const loginUser = false

class Login extends Component {

    state = {
        username: '',
        password: ''
    }

    goToHome = () => {
        this.props.history.push('/home');
    }

    testlogin = () => {
        // console.log(this.state.username)
        // console.log(this.state.password)
        if (this.state.username === 'asdf' && this.state.password === '1234') {
            this.props.addProfile(this.state.username,this.state.password)
            // this.props.history.push('/home');
        }
    }

    onUsernameChange = event => {
        const username = event.target.value;
        this.setState({ username: username })
        // console.log(this.state.username)
    }

    onPasswordChange = event => {
        const password = event.target.value;
        this.setState({ password });
        // console.log(this.state.password)
    }

    showAll = () => {
        console.log("Username:", this.state.username)
        console.log("Password:", this.state.password)
    }

    render() {
        // console.log(this.props)
        // console.log(loginUser)
        return (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <div style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    minHeight: 280,
                    Width: 520,
                    background: '#fff',
                    // width: '40%',
                    height: '100%',
                    marginTop: '10%'
                }}>
                    <div style={{
                        fontSize: 24,
                        padding: 24,
                    }}>
                        Username
                                <Input
                            placeholder="Username"
                            onChange={this.onUsernameChange}
                        ></Input>
                    </div>
                    <div style={{
                        fontSize: 24,
                        padding: 24
                    }}>
                        Password
                                <Input
                            type='password'
                            placeholder="Password"
                            onChange={this.onPasswordChange}
                        ></Input>
                    </div>
                    <div style={{
                        fontSize: 24,
                        padding: 12
                    }}>
                        <Button onClick={this.testlogin}>Login</Button>
                    </div>

                    <div style={{
                        fontSize: 24,
                        padding: 12
                    }}>
                        <Button onClick={this.showAll}>Register</Button>
                    </div>
                </div>
            </div >
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        // addProfile: (email, password, firstname, lastname, token) => {
        //     dispatch({
        //         type: 'ADD_PROFILE',
        //         email: email,
        //         password: password,
        //         firstname: firstname,
        //         lastname: lastname,
        //         token: token
        //     })
        addProfile: (username, password) => {
            dispatch({
                type: 'ADD_PROFILE',
                username: username,
                password: password,
            })
        }
    }
}

export default
    connect(null, mapDispatchToProps)(
        Login
    )