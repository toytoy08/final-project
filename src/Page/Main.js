import React, { Component } from 'react';
import { Layout, Menu, Icon, Spin, Modal, Button, message } from 'antd';
import axios from 'axios'
import ListShop from '../Components/Shop/ListShop'
import DetailShop from '../Components/Shop/DetailShop';

import GoogleMap from '../Components/Map'

class Main extends Component {

    state = {
        shops: [],
        showShop: null,
        showModal: false,
        shopSelected: null,
        lang: 'th',
        shop: [],
        showShopDetail: null,
        showModalDetail: false,
        showMap: null,
        showModalMap: false,
    }

    componentWillMount() {
        axios.get('https://chiangmai.thaimarket.guide/shop', { params: { offset: 0, limit: 100 } })
            .then(response => response.data)
            .then(data => {
                // console.log("DATA Res:", data)
                // console.log("Data.Data Res:", data.data)
                this.setState({ shops: data.data })
                // console.log("State Data", this.state.shops)
            })
    }

    onClickShop = (shop) => {
        this.setState({ shopSelected: shop })
        console.log("Modal ShopSelected:", this.state.shopSelected)
        console.log("Modal before", this.state.showModal)
        this.setState({ showModal: true })
    }

    cancleModal = () => {
        console.log("Modal after", this.state.showModal)
        this.setState({ showModal: false })
    }

    goToLogin = () => {
        this.props.history.push('/login');
    }

    moreDetail = (shopAlone) => {
        // message.info('More Detail', 1);
        axios.get(`https://chiangmai.thaimarket.guide/shop/${shopAlone.id}`)
            .then(response => response.data)
            .then(data => {
                // console.log("DATA Res:", data)
                // console.log("Data.Data Res:", data.data)
                this.setState({ shop: data.data })
                console.log("State Data", this.state.shop)
            })
        // this.props.props.location.state = shopAlone.id
        // const shopid = this.props.props.location.state
        // console.log("Shop Alone:", shopid)
        this.setState({ showModal: false })
        this.setState({ showModalDetail: true })
    }

    cancleModalDetail = () => {
        this.setState({ showModalDetail: false })
    }

    showMap = () => {
        this.setState({ showModalDetail: false })
        this.setState({ showModalMap: true })
    }

    cancleModalMap = () => {
        this.setState({ showModalMap: false })
        this.setState({ showModalDetail: true })
    }

    render() {
        // console.log("Main Props:", this.props)
        // console.log("Inside Main Props:", this.props.props)
        console.log("Data in state after:", this.state.shops)
        return (
            <div style={{ display: 'flex' }} >
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                    <div style={{ textAlign: 'left' }}>Shop</div>
                    {/* {console.log("Data Content:", this.state.shops)} */}
                    {this.state.shops != null ? (
                        <div>
                            <ListShop
                                shops={this.state.shops}
                                onClickShop={this.onClickShop}
                            />
                        </div>
                    ) : (
                            <div>Test</div>
                        )}
                </div>

                {
                    this.state.shopSelected != null ? (
                        <div style={{ marginBottom: '10%' }}>
                            {console.log("Shop Selected:", this.state.shopSelected)}
                            <Modal
                                width="70%"
                                style={{ maxHeight: '50%' }}
                                title={this.state.shopSelected.lang.th.name + ' [ ' + this.state.shopSelected.category + ' ] '}
                                visible={this.state.showModal}
                                onCancel={this.cancleModal}
                                footer={[
                                    <Button
                                        key="moredetail"
                                        icon="plus"
                                        size="large"
                                        shape="round"
                                        onClick={() => { this.moreDetail(this.state.shopSelected) }}
                                    >More Detail</Button>,
                                    <Button
                                        type="primary"
                                        key="close"
                                        icon="cross"
                                        size="large"
                                        shape="round"
                                        onClick={this.cancleModal}
                                    >Close</Button>,
                                ]}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        // marginBottom: '12px'
                                    }}
                                >
                                    <img
                                        src={this.state.shopSelected.image}
                                        style={{ height: '40%', width: '40%' }}
                                        alt=''
                                    />
                                    <div style={{ fontSize: 24, padding: 12 }}>{this.state.shopSelected.lang.th.description}</div>
                                </div>
                            </Modal>
                            <Modal
                                width="90%"
                                style={{ maxHeight: '70%' }}
                                title={this.state.shopSelected.lang.th.name + ' [ ' + this.state.shopSelected.category + ' ] '}
                                visible={this.state.showModalDetail}
                                onCancel={this.cancleModalDetail}
                                footer={[
                                    <Button
                                        key="map"
                                        icon="environment"
                                        size="large"
                                        shape="round"
                                        onClick={this.showMap}
                                    >Map</Button>,
                                    <Button
                                        type="primary"
                                        key="close"
                                        icon="cross"
                                        size="large"
                                        shape="round"
                                        onClick={this.cancleModalDetail}
                                    >Close</Button>,
                                ]}
                            >
                                <DetailShop
                                    shopSelected={this.state.shopSelected}
                                />
                            </Modal>
                            <Modal
                                width="90%"
                                style={{ minHeight: 480 }}
                                title={this.state.shopSelected.lang.th.name + ' [ ' + this.state.shopSelected.category + ' ] '}
                                visible={this.state.showModalMap}
                                onCancel={this.cancleModalMap}
                                footer={[
                                    <Button
                                        type="primary"
                                        key="close"
                                        icon="cross"
                                        size="large"
                                        shape="round"
                                        onClick={this.cancleModalMap}
                                    >Close</Button>,
                                ]}
                            >
                            <div style={{ width: '100%', height: '100%', minHeight: 720, position: 'sticky' }} >

                                <GoogleMap Mainprops={this.state.shopSelected.location} />
                            </div>
                            </Modal>
                        </div>
                    ) : (
                            <div />
                        )
                }
            </div>
        )
    }
}

export default Main