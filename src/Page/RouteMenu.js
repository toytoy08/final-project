import React, { Component } from 'react';
import { Layout, Menu, Icon, BackTop } from 'antd'

import Topbar from '../Layouts/LayoutComponent/Topbar'
import Footerbar from '../Layouts/LayoutComponent/Footerbar'

import Main from './Main'
import Login from './Login'
import Map from '../Components/Map'
import ShopDetail from '../Components/Shop/DetailShop'
import Profile from '../Components/Profile/UserProfile'

const { Sider, Content } = Layout
const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

class RouteMenu extends Component {

    state = {
        index: 0,
        routes: [
            { key: 'main', },
            { key: 'map' },
            { key: 'profile', },
            { key: 'login', },
            // { key: 'shop detail', },
        ],
    }

    renderPage = (route) => {
        // console.log("Render Func:", route)
        // console.log("Render Key Func:", route.key)
        switch (route) {
            case 'main':
                return <Main props={this.props} />
            case 'map':
                return <Map />
            case 'profile':
                return <Profile />
            case 'login':
                return <Login />
            // case 'shop detail':
            //     return <ShopDetail />
        }
    }

    onMenuClick = (keyName) => {
        // console.log(keyName)
        // console.log(keyName.key)
        this.setState({ index: keyName.key })
        // if (keyName.key) {
        //     var path = '/'
        //     path = `/${keyName.key}`
        //     this.props.history.replace(path)
        // }
    }

    getStrUpper = (str) => {
        var up = str.toUpperCase()
        return up
    }

    render() {
        // console.log("Route Menu Props:", this.props)
        // console.log("Local Props:", this.props.location.state)
        // console.log("Route Menu Key Props:", this.state.routes)
        // console.log("Index:", this.state.index);
        // if (this.props.location.state != null) {
        //     console.log("Local Props:", this.props.location.state)
        //     this.renderPage(this.state.routes[4].key)
        // }
        return (
            <div style={{ textAlign: 'center' }}>
                <Layout>
                    <Topbar
                        style={{ top: 0, width: '100%' }}
                        textHeader={this.getStrUpper(this.state.routes[this.state.index].key)}
                    />
                    <Layout>
                        <Sider style={{ left: 0 }}>
                            <Menu
                                onClick={this.handleClick}
                                style={{ width: '100%', height: '100%' }}
                                defaultSelectedKeys={[(this.state.index).toString()]}
                                defaultOpenKeys={['sub1','sub2']}
                                mode="inline"
                                onClick={keyName => {
                                    this.onMenuClick(keyName);
                                }}
                            >
                                <SubMenu key="sub1" title={<span><Icon type="home" /><span>Manu</span></span>}>
                                    <Menu.Item key="0">Main</Menu.Item>
                                    <Menu.Item key="1">Map</Menu.Item>
                                    {/* <Menu.Item key="4">Shop Detail</Menu.Item> */}
                                </SubMenu>
                                <SubMenu key="sub2" title={<span><Icon type="user" /><span>User</span></span>}>
                                    <Menu.Item key="2">Profile</Menu.Item>
                                    <Menu.Item key="3">Login</Menu.Item>
                                </SubMenu>
                            </Menu>
                        </Sider>
                        <Content style={{ width: '100%', height: '100%', minHeight: 720,position:'sticky' }}>
                            {this.renderPage(this.state.routes[this.state.index].key)}
                            <BackTop />
                        </Content>
                    </Layout>
                    <Footerbar
                        style={{ bottom: 0 }}
                        textFooter="Project CMU Guide ©2019 Created by Napapat Mokamol"
                    />
                </Layout>
            </div >
        )
    }
}

export default RouteMenu