import React, { Component } from 'react'
import { Card, List, Rate, Comment, Avatar, Input, Form, Button, message } from 'antd'
import axios from 'axios'
import moment from 'moment';

import GoogleMap from '../Map'

const { Meta } = Card

const TextArea = Input.TextArea;

const CommentList = ({ comments }) => (
    <List
        dataSource={comments}
        header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
        itemLayout="horizontal"
        renderItem={props => <Comment {...props} />}
    />
);

const Editor = ({
    onChange, onSubmit, submitting, value
}) => (
        <div>
            <Form.Item>
                <TextArea rows={4} onChange={onChange} value={value} />
                <Rate />
            </Form.Item>
            <Form.Item>
                <Button
                    htmlType="submit"
                    loading={submitting}
                    onClick={onSubmit}
                    type="primary"
                >
                    Add Comment
      </Button>
            </Form.Item>
        </div>
    );

class DetailShop extends Component {

    state = {
        shop: [],
        comments: [],
        submitting: false,
        value: '',
        valueStar: null,
    }

    handleSubmit = () => {
        if (!this.state.value) {
            return;
        }

        else if (this.state.value) {
            this.setState({
                submitting: true,
            });

            setTimeout(() => {
                this.setState({
                    submitting: false,
                    value: '',
                    valueStar: null,
                    comments: [
                        {
                            author: 'Han Solo',
                            avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
                            content: <p>{this.state.value}</p>,
                            datetime: moment().fromNow(),
                            star: this.state.valueStar,
                        },
                        ...this.state.comments,
                    ],
                })
            }, 1000)
        }
        else {
            message.info('Please Submit comment and rate', 1)
        }
    }

    handleCommentChange = (e) => {
        console.log("Comment event:",e)
        this.setState({
            value: e.target.value
        });
        console.log("Comment Value:", e.target.value)
    }

    handleStarChange = (value) => {
        this.setState({ valueStar: value })
        console.log("Point:", value)
    }

    componentWillReceiveProps() {
        axios.get(`https://chiangmai.thaimarket.guide/shop/${this.props.shopSelected.id}`)
            .then(response => response.data)
            .then(data => {
                // console.log("DATA Res:", data)
                // console.log("Data.Data Res:", data.data)
                this.setState({ shop: data.data })
                // console.log("State Data", this.state.shop)
            })
    }

    render() {
        // console.log("ListShop props:", this.props)
        // console.log("Comment props:", this.state.comments)
        console.log("Shop Name:", this.state.shop)
        const { comments, submitting, value } = this.state;
        const actions = <span>Reply to</span>
        return (
            <div style={{ Height: '480px', textAlign: 'center' }}>
                {this.state.shop != null ? (
                    <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                        {/* Shop : {this.state.shop.lang.th} */}
                        <img src={this.state.shop.image} style={{ height: '30%', width: '30%', marginBottom: 16 }} />
                        <List
                            // pagination={{ pageSize: 5, alignItems: 'left' }}
                            grid={{ gutter: 16, column: 4 }}
                            dataSource={this.state.shop.products}
                            renderItem={item => (
                                <List.Item>
                                    <Card
                                        hoverable
                                        cover={<img
                                            alt=''
                                            src={
                                                item.image
                                            }
                                            style={{
                                                Height: '100%',
                                                minHeight: 120
                                            }}
                                        />}
                                    >
                                        <div style={{ textAlign: 'center' }}>
                                            {console.log("Category:", this.state.shop.category)}
                                            <Meta
                                                title={item.lang.th.name}
                                                description={item.lang.th.description}
                                            />
                                        </div>
                                    </Card>
                                </List.Item>
                            )}
                        />
                        {/* <Comment
                        actions={actions}
                        author={<a>Han Solo</a>}
                        avatar={(
                            <Avatar
                                src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                                alt="Han Solo"
                            />
                        )}
                        content={(
                            <p>We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.</p>
                        )}
                        datetime={(
                            <Tooltip title={moment().format('YYYY-MM-DD HH:mm:ss')}>
                                <span>{moment().fromNow()}</span>
                            </Tooltip>
                        )}
                    /> */}
                        <div>
                            {comments.length > 0 && <CommentList comments={comments} />}
                            <Comment
                                avatar={(
                                    <Avatar
                                        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                                        alt="Han Solo"
                                    />
                                )}

                                content={
                                    (
                                        <Editor
                                            onChange={this.handleCommentChange}
                                            onSubmit={this.handleSubmit}
                                            submitting={submitting}
                                            value={value}
                                        />
                                    )}
                            />
                        </div>
                    </div>
                ) : (<div />)}
            </div>
        )
    }
}

export default DetailShop;