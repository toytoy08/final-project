import React, { Component } from 'react';
import { Card, Spin, Icon } from 'antd';

const antIcon = <Icon type="loading" style={{ fontSize: 96 }} spin />

const { Meta } = Card;

class Itemshop extends Component {

    state = {
        shopForImage: []
    }

    // loadImage = (shopid) => {
    //     // console.log("Shopid:", shopid)
    //     axios.get(`https://chiangmai.thaimarket.guide/shop/${shopid}`)
    //         .then(response => response.data)
    //         .then(data => {
    //             // console.log("DATA Res:", data)
    //             // console.log("Image data:", data.data.image)
    //             this.setState({ shopForImage: data.data.image })
    //         })
    //     return this.state.shopForImage
    // }

    render() {
        // console.log("ItemShop props:", this.props)
        // console.log("Load data", this.props.item)
        //         console.log("Data.Data Res:", data.data)
        //         this.setState({ shop: data.data })
        //         console.log("State Data", this.state.shopForImage)
        return (
            <div style={{ Height: '480px' }}>
                {this.props !== null ? (
                    <Card
                        hoverable
                        cover={<img
                            alt=''
                            src={
                                this.props.item.image
                            }
                            style={{
                                minHeight: '160px'
                            }}
                            onClick={() => {
                                this.props.onClickShop(this.props.item)
                            }}
                        />}
                    >
                        <div style={{ textAlign: 'center' }}>
                            <Meta
                                title={this.props.item.lang.th.name}
                                description={this.props.item.category}
                            />
                        </div>
                    </Card>
                ) : (
                        <div style={{ textAlign: 'center' }}>
                            <Spin indicator={antIcon} />
                        </div>
                    )}
            </div>

        )
    }
}

export default Itemshop;