import React, { Component } from 'react';
import Itemshop from './ItemShop';
import { List } from 'antd';

class Listshop extends Component {

  render() {
    // console.log("ListShop props:", this.props)
    return (
      <div style={{ Height: '480px' }}>
        <List
          pagination={{ pageSize: 12, alignItems: 'center' }}
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.props.shops}
          renderItem={item => (
            <List.Item>
              <Itemshop
                item={item}
                onClickShop={this.props.onClickShop}
              />
            </List.Item>
          )}
        />
      </div>
    )
  }
}


export default Listshop;
