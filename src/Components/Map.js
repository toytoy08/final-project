import React, { Component } from 'react'
import axios from 'axios'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'
import retail from '../Templatic-map-icons/retail-stores.png'
import shopping from '../Templatic-map-icons/shopping.png'
import restaurants from '../Templatic-map-icons/restaurants.png'
import coffee from '../Templatic-map-icons/coffee-n-tea.png'
import matrimonial from '../Templatic-map-icons/matrimonial.png'
import community from '../Templatic-map-icons/community.png'
import exhibitions from '../Templatic-map-icons/exhibitions.png'
import furniture from '../Templatic-map-icons/furniture-stores.png'
import manufacturing from '../Templatic-map-icons/manufacturing.png'
import medical from '../Templatic-map-icons/medical.png'
import fashion from '../Templatic-map-icons/fashion.png'
import { Spin, Icon, Modal } from 'antd'

import DetailShop from './Shop/DetailShop'

const antIcon = <Icon type="loading" style={{ fontSize: 96 }} spin />

class GoogleMap extends Component {

    state = {
        data: [],
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {},
        locationData: [],
        mapVisible: false,
        showShopDetail: null,
        showModalDetail: false,
        shopSelected: null,
        markerId: null,
        location: null,
        zoomlevel: null
    }

    componentWillMount() {
        axios.get('https://chiangmai.thaimarket.guide/shop', { params: { offset: 0, limit: 100 } })
            .then(response => response.data)
            .then(data => {
                // console.log("DATA Res:", data)
                // console.log("Data.Data Res:", data.data)
                this.setState({ data: data.data })
                // this.forceUpdate()
            })
            .then(this.loadLocation)
            .then(this.loadData)
    }

    loadData = () => {
        // console.log("Data Load:", data, index)
        this.state.data.forEach((element, index) => {
            // console.log("Element:", element, index)
            // console.log("Item:", element.location)
            this.setState({
                locationData: [...this.state.locationData, {
                    title: element.lang.th.name,
                    position: {
                        lat: parseFloat(element.location.latitude),
                        lng: parseFloat(element.location.longitude)
                    },
                    category: element.category,
                    locationMark: this.markerIcon(element.category),
                    id: element.id
                }]
            })
            // this.markerIcon(this.state.locationData[index].category)
        })
        console.log("Local state:", this.state.locationData)
        this.setState({ mapVisible: true })
    }

    loadLocation = () => {
        console.log("From Main props:", this.props.Mainprops)
        if (this.props.Mainprops) {
            this.setState({
                location: {
                    lat: parseFloat(this.props.Mainprops.latitude),
                    lng: parseFloat(this.props.Mainprops.longitude)
                },
                zoomlevel: 20
            })
            console.log("LoadLocation:", this.state.location)
        }
        else {
            this.setState({
                location: {
                    lat: 18.802587,
                    lng: 98.9529
                },
                zoomlevel: 15
            })
            console.log("LoadLocation:", this.state.location)
        }
    }

    onMarkerClick = (props, marker, e) => {
        console.log("Props:", props)
        // console.log("Marker:", marker)
        // console.log("Event:", e)
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true,
            markerId: props.children
        })
        // console.log("Id:", this.state.markerId)
    }

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            })
        }
    }

    fetchPlaces(mapProps, map) {
        const { google } = mapProps;
        const service = new google.maps.places.PlacesService(map);
        console.log("Map Props:", mapProps)
        console.log("Map:", map)
        console.log("Google Props:", google)
        console.log("Service Props:", service)
    }

    markerIcon = (category) => {
        console.log("Category:", category)
        switch (category) {
            case 'ร้านค้า':
            case 'ตลาด':
                return retail

            case 'ศูนย์การค้า':
                return shopping

            case 'อาหารไทย':
            case 'อาหารเอเชีย':
            case 'ร้านอาหาร (และเครื่องดื่ม)':
                return restaurants

            case 'คาเฟ่และของหวาน':
                return coffee

            case 'เครื่องประดับ':
                return matrimonial

            case 'บริการต่างๆ':
                return community

            case 'สินค้าเบ็ดเตล็ด และอื่นๆ':
            case 'ของที่ระลึก':
                return exhibitions

            case 'เฟอร์นิเจอร์และของตกแต่งบ้าน':
            case 'ต้นไม้และอุปกรณ์ทำสวน':
                return furniture

            case 'ของเก่า ของสะสม':
                return manufacturing

            case 'สุขภาพและความงาม':
                return medical

            case 'แฟชั่นสตรี':
            case 'แฟชั่นบุรุษ':
                return fashion

            default:
                return ''
        }
    }

    moreDetail = (id) => {
        console.log("Id marker detail:", id)
        this.setState({ shopSelected: id })
        console.log("Modal ShopSelected:", this.state.shopSelected)
        // axios.get(`https://chiangmai.thaimarket.guide/shop/${shopAlone.id}`)
        //     .then(response => response.data)
        //     .then(data => {
        //         // console.log("DATA Res:", data)
        //         // console.log("Data.Data Res:", data.data)
        //         this.setState({ shop: data.data })
        //         console.log("State Data", this.state.shop)
        //     })
        this.setState({ showModalDetail: true })
    }

    cancleModalDetail = () => {
        this.setState({ showModalDetail: false })
    }

    render() {
        // console.log("Map:", this.props)
        // console.log("Mainprops:", this.props.Mainprops)
        // console.log("Location Load:", this.state.locationData)
        return (
            <div >
                {this.state.mapVisible !== false ? (
                    <Map
                        onClick={this.onMapClicked}
                        google={this.props.google}
                        zoom={this.state.zoomlevel}
                        initialCenter={{
                            lat: this.state.location.lat,
                            lng: this.state.location.lng
                        }}
                        visible={this.state.mapVisible}
                    // onReady={this.fetchPlaces}
                    >
                        {this.state.locationData.map((marker, index) => (
                            < Marker
                                key={index}
                                name={marker.title}
                                position={marker.position}
                                onClick={this.onMarkerClick}
                                icon={{
                                    url: marker.locationMark
                                }}
                                children={marker.id}
                            />
                        ))}
                        < InfoWindow
                            marker={this.state.activeMarker}
                            visible={this.state.showingInfoWindow}
                        >
                            <div>
                                <h2>{this.state.selectedPlace.name}</h2>
                                {/* <Button onClick={this.moreDetail(this.state.markerId)}>More Detail</Button> */}
                            </div>
                        </InfoWindow>
                    </Map >
                ) : (
                        <div style={{ textAlign: 'center' }}>
                            <Spin indicator={antIcon} />
                        </div>
                    )}
                {/* {this.state.shopSelected !== null ? (
                    <Modal
                        width="90%"
                        style={{ maxHeight: '70%' }}
                        title={this.state.selectedPlace.name}
                        visible={this.state.showModalDetail}
                        onCancel={this.cancleModalDetail}
                        footer={[
                            <Button
                                type="primary"
                                key="close"
                                icon="cross"
                                size="large"
                                shape="round"
                                onClick={this.cancleModalDetail}
                            >Close</Button>,
                        ]}
                    >
                        <DetailShop
                            shopSelected={this.state.shopSelected}
                        />
                    </Modal>
                ) : (
                        <div />
                    )} */}
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyB_Mcr9VWBFHvu1I94jH3LFk5Fw_j7cT44')
})(GoogleMap)