import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

// import MainPage from './Page/Main'
import Login from './Page/Login'
// import ShopDetail from './Components/Shop/DetailShop'
// import Profile from './Components/Profile/UserProfile'
// import CommentAndRate from './Components/CommentAndRate'

// import GoogleMap from './Components/Map'

import RouteMenu from './Page/RouteMenu'

import { Provider } from 'react-redux'
import { store, history } from './Reducer/Store'
import { ConnectedRouter } from 'connected-react-router';


function Routes() {
    return (
        <div>
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Redirect from="/" exact to="/home" />
                        <Route path="/home" exact component={RouteMenu} />
                        {/* <Route path="/home" exact component={MainPage} /> */}
                        <Route path="/login" exact component={Login} />
                        {/* <Route path="/shop/detail" exact component={ShopDetail} />
                        <Route path="/profile" exact component={Profile} />
                        <Route path="/comment" exact component={CommentAndRate} />
                        <Route path="/map" exact component={GoogleMap} /> */}
                    </Switch>
                </ConnectedRouter>
            </Provider>
        </div>
    );
}

export default Routes;
