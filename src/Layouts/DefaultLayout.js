import React, { Component } from 'react';
import Topbar from '../Layouts/LayoutComponent/Topbar'
import Siderbar from './LayoutComponent/Sidebar'
import { Layout } from 'antd';
import Footerbar from './LayoutComponent/Footerbar'

const { Content } = Layout

const DefaultLayout = ({ textHeader, children, textFooter, path }) => (

    <div>
        <Topbar textHeader={{ textHeader }} />
        <Layout>
            <Siderbar path={{ path }} />
        </Layout>
        <Footerbar textFooter={{ textFooter }} />
    </div>

)

export default DefaultLayout