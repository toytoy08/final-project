import React, { Component } from 'react';
import { Layout } from 'antd'

const { Header } = Layout

class Topbar extends Component {
    render() {
        // console.log("Topbar Props",this.props)
        const textHeader = this.props.textHeader
        return (
            <Header className="header" >
                <div style={{ textAlign: 'left', color: 'white', fontSize: 36 }}>
                    {textHeader}
                </div>
            </Header>
        )
    }
}

export default Topbar