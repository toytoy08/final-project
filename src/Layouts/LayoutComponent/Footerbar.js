import React, { Component } from 'react';
import { Layout } from 'antd'

const { Footer } = Layout

class Footerbar extends Component {
    render() {
        // console.log("Footerbar Props:", this.props)
        const textFooter = this.props.textFooter
        return (
            <Footer style={{ textAlign: 'center', marginBottom: 16, fontSize: 24, fontWeight: 'bold' }}>
                {textFooter}
            </Footer>
        )
    }
}

export default Footerbar