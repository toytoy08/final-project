import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd'

const { Sider } = Layout
const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup

class Sidebar extends Component {

    onMenuClick = (e) => {
        // console.log(e)
        // console.log(e.key)
        if (e.key) {
            var path = '/'
            path = `/${e.key}`
            this.props.path.history.replace(path)
        }
    }

    render() {
        // console.log("Sider Props:", this.props)
        // console.log("Key Props:", this.props.atKey)
        return (
            <Sider>
                <Menu
                    onClick={this.handleClick}
                    style={{ width: '100%', height: '100%' }}
                    defaultSelectedKeys={[this.props.atKey]}
                    defaultOpenKeys={['sub1']}
                    mode="inline"
                    onClick={e => {
                        this.onMenuClick(e);
                    }}
                >
                    <SubMenu key="sub1" title={<span><Icon type="home" /><span>Manu</span></span>}>
                        {/* <MenuItemGroup key="g1" title="Item 1"> */}
                        <Menu.Item key="home">Main</Menu.Item>
                        <Menu.Item key="map">Map</Menu.Item>
                        {/* </MenuItemGroup>
                        <MenuItemGroup key="g2" title="Item 2">
                            <Menu.Item key="3">Option 3</Menu.Item>
                            <Menu.Item key="4">Option 4</Menu.Item>
                        </MenuItemGroup> */}
                    </SubMenu>
                    {/* <SubMenu key="sub2" title={<span><Icon type="appstore" /><span>Navigation Two</span></span>}>
                        <Menu.Item key="5">Option 5</Menu.Item>
                        <Menu.Item key="6">Option 6</Menu.Item>
                        <SubMenu key="sub3" title="Submenu">
                            <Menu.Item key="7">Option 7</Menu.Item>
                            <Menu.Item key="8">Option 8</Menu.Item>
                        </SubMenu>
                    </SubMenu> */}
                    <SubMenu key="sub4" title={<span><Icon type="user" /><span>User</span></span>}>
                        <Menu.Item key="profile">Profile</Menu.Item>
                        <Menu.Item key="login">Login</Menu.Item>
                    </SubMenu>
                </Menu >
            </Sider>
        )
    }
}

export default Sidebar