import React, { Component } from 'react';
import { Layout } from 'antd'

const { Content } = Layout

class Contentarea extends Component {
    render() {
        // console.log(this.props)
        return (
            <Content style={{ padding: '24px', minHeight: 280 }}>
                <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                    content
                </div>
            </Content>
        )
    }
}

export default Contentarea